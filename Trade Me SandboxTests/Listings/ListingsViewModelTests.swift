//
//  ListingsViewModelTests.swift
//  Trade Me SandboxTests
//
//  Created by Jose Solorzano on 8/13/18.
//  Copyright © 2018 Jose Solorzano. All rights reserved.
//

import Foundation
import Nimble
import Quick
import Moya
import RxSwift
import RxBlocking

@testable import Trade_Me_Sandbox

class ListingsViewModelTests: QuickSpec {
    
    override func spec() {
        let bag = DisposeBag()
        let rootCategory = Category.root()
        let loggingPlugin = NetworkLoggerPlugin(verbose: true, cURL: true)
        let networkProvider : MoyaProvider<TradeMeSandbox> = MoyaProvider<TradeMeSandbox>(stubClosure : MoyaProvider.immediatelyStub, plugins: [loggingPlugin])
        
        NetworkingManager.sharedInstance.bundle = Bundle(for: type(of: self))
        
        describe("Test the listings view model") {
            
            let viewModel = ListingsViewModel(provider: networkProvider)
            viewModel.category = rootCategory
            
            context("Created a debug view model and fetched root listings") {
                
                it("Has populated listings") {
                    
                    var listings : [Trade_Me_Sandbox.Listing] = [Trade_Me_Sandbox.Listing]()
                    
                    viewModel.currentListings.subscribe(onNext: { received in
                        listings = received
                    }).disposed(by : bag)
                    
                    viewModel.reload(forced: true)
                    self.wait(for: 2)
                    
                    expect(listings).toNot(beEmpty())
                }
            }
        }
    }
}

//
//  CategoriesViewControllerTests.swift
//  Trade Me SandboxTests
//
//  Created by Jose Solorzano on 8/14/18.
//  Copyright © 2018 Jose Solorzano. All rights reserved.
//

import Foundation
import Nimble
import Quick
import Moya
import RxSwift
import RxBlocking

@testable import Trade_Me_Sandbox

class ListingsViewControllerTests : QuickSpec {
    
    override func spec() {
        let window = UIWindow()
        let loggingPlugin = NetworkLoggerPlugin(verbose: true, cURL: true)
        let networkProvider : MoyaProvider<TradeMeSandbox> = MoyaProvider<TradeMeSandbox>(stubClosure : MoyaProvider.immediatelyStub, plugins: [loggingPlugin])
        
        NetworkingManager.sharedInstance.bundle = Bundle(for: type(of: self))
        
        describe("Test a listing view controller") {
            let rootCategory = Category.root()
            let emptyCategory = Category.empty()

            context("Created a view controller") {
                
                let viewModel = ListingsViewModel(provider: networkProvider)
                let viewController = ListingsViewController()
                viewController.viewModel = viewModel
                viewModel.category = rootCategory
                window.addSubview(viewController.view)
                RunLoop.current.run(until: Date())
                self.wait(for: 1)
                
                it("Binds title") {
                    expect(viewController.navigationItem.title).to(equal(rootCategory.name))
                }
            }
            
            context("Created a view controller with a category without listings") {
                
                let viewModel = ListingsViewModel(provider: networkProvider)
                let viewController = ListingsViewController()
                viewController.viewModel = viewModel
                viewModel.category = emptyCategory
                window.addSubview(viewController.view)
                RunLoop.current.run(until: Date())
                self.wait(for: 1)
                
                it("Reloads and displays no results") {
                    expect(viewController.collectionView.visibleCells.count).to(be(0))
                }
            }
        }
    }
}

//
//  CategoriesViewControllerTests.swift
//  Trade Me SandboxTests
//
//  Created by Jose Solorzano on 8/14/18.
//  Copyright © 2018 Jose Solorzano. All rights reserved.
//

import Foundation
import Nimble
import Quick
import Moya
import RxSwift
import RxBlocking

@testable import Trade_Me_Sandbox

class MockCategoriesDelegate : CategoryNavigationDelegate {
    
    var didChooseCategory : Bool = false
    var didDisplayListing : Bool = false
    func did(choose: Trade_Me_Sandbox.Category) { self.didChooseCategory = true  }
    func showFilters() { }
    func hideFilters() { }
    func display(_ listing: Listing) { self.didDisplayListing = true }
}

class CategoriesViewControllerTests : QuickSpec {
    
    override func spec() {
        let window = UIWindow()
        let rootCategory = Category.root()
        let loggingPlugin = NetworkLoggerPlugin(verbose: true, cURL: true)
        let networkProvider : MoyaProvider<TradeMeSandbox> = MoyaProvider<TradeMeSandbox>(stubClosure : MoyaProvider.immediatelyStub, plugins: [loggingPlugin])
        
        NetworkingManager.sharedInstance.bundle = Bundle(for: type(of: self))
        
        describe("Test a category list controller") {
            
            context("Created a test view controller") {
                
                let viewModel = CategoryFilteringViewModel(category: rootCategory, provider: networkProvider)
                let viewController = CategoryListViewController()
                let delegate = MockCategoriesDelegate()
                viewController.navigationDelegate = delegate
                viewController.viewModel = viewModel
                window.addSubview(viewController.view)
                RunLoop.current.run(until: Date())
                self.wait(for: 1)

                it("Reloads and displays the results") {
                    expect(viewController.tableView.visibleCells.count).to(beGreaterThan(0))
                }
                
                it("Binds title") {
                    expect(viewController.navigationItem.title).to(equal(rootCategory.name))
                }
            }
        }
    }
}

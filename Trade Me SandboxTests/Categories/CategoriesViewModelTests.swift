//
//  CategoriesTest.swift
//  Trade Me SandboxTests
//
//  Created by Jose Solorzano on 8/13/18.
//  Copyright © 2018 Jose Solorzano. All rights reserved.
//

import Foundation
import Nimble
import Quick
import Moya
import RxSwift
import RxBlocking

@testable import Trade_Me_Sandbox

class CategoriesViewModelTests: QuickSpec {
    
    override func spec() {
        let bag = DisposeBag()
        let rootCategory = Category.root()
        let loggingPlugin = NetworkLoggerPlugin(verbose: true, cURL: true)
        let networkProvider : MoyaProvider<TradeMeSandbox> = MoyaProvider<TradeMeSandbox>(stubClosure : MoyaProvider.immediatelyStub, plugins: [loggingPlugin])
        
        NetworkingManager.sharedInstance.bundle = Bundle(for: type(of: self))
        
        describe("Test the category view model") {
            
            let viewModel = CategoryFilteringViewModel(category: rootCategory, provider: networkProvider)
            
            context("Created a debug view model and fetched root categories") {
                
                it("Has populated subcategories") {
                    
                    var subcategories : [Trade_Me_Sandbox.Category] = [Trade_Me_Sandbox.Category]()
                    
                    viewModel.currentSubcategories.subscribe(onNext: { categories in
                        subcategories = categories
                    }).disposed(by : bag)
                    
                    viewModel.reload(forced: true)
                    self.wait(for: 2)
                    
                    expect(subcategories).toNot(beEmpty())
                }
            }
        }
    }
}

//
//  XCTestCase+TradeMe.swift
//  Trade Me SandboxTests
//
//  Created by Jose Solorzano on 8/13/18.
//  Copyright © 2018 Jose Solorzano. All rights reserved.
//

import Foundation
import XCTest

extension XCTestCase {
    
    func wait(for duration: TimeInterval) {
        let waitExpectation = expectation(description: "Waiting")
        
        let when = DispatchTime.now() + duration
        DispatchQueue.main.asyncAfter(deadline: when) {
            waitExpectation.fulfill()
        }
        
        // We use a buffer here to avoid flakiness with Timer on CI : namely Bitrise
        waitForExpectations(timeout: duration + 0.5)
    }
}

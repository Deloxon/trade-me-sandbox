//
//  CategoriesViewControllerTests.swift
//  Trade Me SandboxTests
//
//  Created by Jose Solorzano on 8/14/18.
//  Copyright © 2018 Jose Solorzano. All rights reserved.
//

import Foundation
import Nimble
import Quick
import Moya
import RxSwift
import RxBlocking

@testable import Trade_Me_Sandbox

class DetailedListingViewControllerTests : QuickSpec {
    
    override func spec() {
        let window = UIWindow()
        let loggingPlugin = NetworkLoggerPlugin(verbose: true, cURL: true)
        let networkProvider : MoyaProvider<TradeMeSandbox> = MoyaProvider<TradeMeSandbox>(stubClosure : MoyaProvider.immediatelyStub, plugins: [loggingPlugin])
        
        NetworkingManager.sharedInstance.bundle = Bundle(for: type(of: self))
        
        describe("Test a detailed listing view controller") {
            let listing = Listing(id: 0)
            let viewModel = ListingDetailViewModel(listing: listing, provider: networkProvider)

            context("Created a view controller") {
                
                let viewController = ListingDetailViewController()
                viewController.viewModel = viewModel
                window.addSubview(viewController.view)
                RunLoop.current.run(until: Date())
                self.wait(for: 1)
                
                it("Binds title") {
                    expect(viewController.informationView.titleLabel.text).to(equal(viewModel.title.value))
                }
                
                it("Binds reserve text") {
                    expect(viewController.informationView.reserveLabel.text).to(equal(viewModel.reserveText.value))
                }
            }
        }
    }
}

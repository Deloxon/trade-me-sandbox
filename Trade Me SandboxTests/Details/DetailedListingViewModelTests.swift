//
//  DetailedListingViewModel.swift
//  Trade Me SandboxTests
//
//  Created by Jose Solorzano on 8/13/18.
//  Copyright © 2018 Jose Solorzano. All rights reserved.
//

//
//  CategoriesTest.swift
//  Trade Me SandboxTests
//
//  Created by Jose Solorzano on 8/13/18.
//  Copyright © 2018 Jose Solorzano. All rights reserved.
//

import Foundation
import Nimble
import Quick
import Moya
import RxSwift
import RxBlocking

@testable import Trade_Me_Sandbox

class DetailedListingViewModelTests: QuickSpec {
    
    override func spec() {
        let bag = DisposeBag()
        let fakeListing = Listing.fake()
        let loggingPlugin = NetworkLoggerPlugin(verbose: true, cURL: true)
        let networkProvider : MoyaProvider<TradeMeSandbox> = MoyaProvider<TradeMeSandbox>(stubClosure : MoyaProvider.immediatelyStub, plugins: [loggingPlugin])
        
        NetworkingManager.sharedInstance.bundle = Bundle(for: type(of: self))
        
        describe("Test the detailed listing view model") {
            
            let viewModel = ListingDetailViewModel(listing: fakeListing, provider: networkProvider)
            
            context("Created a debug view model and fetch the details of a listing") {
                
                it("Has populated title") {
                    
                    var title : String = ""
                    
                    viewModel.title.asObservable().subscribe(onNext: { received in
                        title = received
                    }).disposed(by: bag)
                    
                    viewModel.reload()
                    self.wait(for: 2)
                    
                    expect(title).toNot(beEmpty())
                }
            }
        }
    }
}

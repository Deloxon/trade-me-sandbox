//
//  UIFont+TradeMe.swift
//  Trade Me Sandbox
//
//  Created by Jose Solorzano on 8/11/18.
//  Copyright © 2018 Jose Solorzano. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
    class func smallListingFont() -> UIFont {
        return UIFont.systemFont(ofSize: 10, weight: .thin)
    }
    
    class func mediumListingFont() -> UIFont {
        return UIFont.systemFont(ofSize: 14, weight: .thin)
    }
    
    class func boldListingFont() -> UIFont {
        return UIFont.systemFont(ofSize: 14, weight: .medium)
    }
    
    class func detailTitleFont() -> UIFont {
        return UIFont.systemFont(ofSize: 18, weight: .thin)
    }
    
    class func detailPriceFont() -> UIFont {
        return UIFont.systemFont(ofSize: 18, weight: .medium)
    }
}

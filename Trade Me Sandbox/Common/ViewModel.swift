//
//  ViewModel.swift
//  Trade Me Sandbox
//
//  Created by Jose Solorzano on 8/12/18.
//  Copyright © 2018 Jose Solorzano. All rights reserved.
//

import Foundation
import RxSwift
import Moya 
enum LoadingStatus {
    case loading
    case notLoading
}

class ViewModel {
    
    var networkProvider : MoyaProvider<TradeMeSandbox>

    init(provider : MoyaProvider<TradeMeSandbox>) {
        self.networkProvider = provider
    }
    
    var mainLoadingStatusVariable = Variable<LoadingStatus>(.notLoading)
    public var mainLoadingStatus : Observable<LoadingStatus> {
        return mainLoadingStatusVariable.asObservable()
    }
}

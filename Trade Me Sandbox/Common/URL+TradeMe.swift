//
//  URL+TradeMe.swift
//  Trade Me Sandbox
//
//  Created by Jose Solorzano on 8/12/18.
//  Copyright © 2018 Jose Solorzano. All rights reserved.
//

import Foundation
import UIKit

extension URL {
    static func from(_ photo : Photo) -> URL {
        return URL(string: "https://images.tmsandbox.co.nz/photoserver/full/\(photo.key).jpg") ?? URL(string: "")!
    }
}

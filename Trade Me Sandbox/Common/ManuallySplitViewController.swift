//
//  ViewController.swift
//  Trade Me Sandbox
//
//  Created by Jose Solorzano on 8/9/18.
//  Copyright © 2018 Jose Solorzano. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa

/**
 Manual implementation of a Split View Controller
 - Parameters:
    - master: View controller pinned to the left, in charge of dictating navigation
    - detail: View controller pinned to the right, usually displaying results
 */
class ManuallySplitViewController: UIViewController {
    
    public var masterPercentage : CGFloat = 0.35
    private var masterViewController : UIViewController!
    private var detailViewController : UIViewController!
    
    convenience init(master : UIViewController, detail : UIViewController) {
        self.init(nibName: nil, bundle: nil)
        self.masterViewController = master
        self.detailViewController = detail
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addSubview(detailViewController.view)
        self.addChildViewController(detailViewController)
        
        self.view.addSubview(masterViewController.view)
        self.addChildViewController(masterViewController)
        self.masterViewController.view.addShadow(level : .low)
        
        let orientation = UIDevice.current.orientation
        let startupFakeSize = orientation.isPortrait ? CGSize(width: 1, height: 2) : CGSize(width: 2, height: 1)
        self.calculateMasterWidth(with : startupFakeSize)
        self.setConstraints()
        
        masterViewController.didMove(toParentViewController: self)
        detailViewController.didMove(toParentViewController: self)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        self.calculateMasterWidth(with : size)
        setConstraints()
        detailViewController.viewWillTransition(to : size , with : coordinator)
    }
    
    ///Calculate how much space the master controller will occupy, depending on current pad trait
    private func calculateMasterWidth(with size : CGSize) {
        if size.width > size.height {
            masterPercentage = 0.35
        } else {
            masterPercentage = 0.5
        }
    }
    
    ///Setup constraints for each view controller
    private func setConstraints() {
        guard let superview = self.view else { return }
        
        masterViewController.view.snp.remakeConstraints { maker in
            maker.left.equalTo(superview)
            maker.top.equalTo(superview)
            maker.bottom.equalTo(superview)
            maker.width.equalTo(superview).multipliedBy(self.masterPercentage)
        }
        
        detailViewController.view.snp.remakeConstraints { maker in
            maker.right.equalTo(superview)
            maker.top.equalTo(superview)
            maker.bottom.equalTo(superview)
            maker.width.equalTo(superview).multipliedBy(1.0 - self.masterPercentage)
        }
    }
}


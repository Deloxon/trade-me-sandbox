//
//  Array+TradeMe.swift
//  Trade Me Sandbox
//
//  Created by Jose Solorzano on 8/12/18.
//  Copyright © 2018 Jose Solorzano. All rights reserved.
//

import Foundation

extension Array where Element : Listing {
    var limitedAmount : Int { return 20 }
    func limit() -> Array<Listing> {
        if self.count > limitedAmount { return Array(self[0...limitedAmount])}
        return self
    }
}

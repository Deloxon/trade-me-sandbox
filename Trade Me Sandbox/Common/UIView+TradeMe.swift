//
//  UIView+TradeMe.swift
//  Trade Me Sandbox
//
//  Created by Jose Solorzano on 8/11/18.
//  Copyright © 2018 Jose Solorzano. All rights reserved.
//

import Foundation
import UIKit

enum ShadowLevel {
    case low,
    mid,
    high
    
    var radius : CGFloat {
        switch self {
        case .low:
            return 4
        case .mid:
            return 6
        default:
            return 8
        }
    }
}

extension UIView {
    func addShadow(level : ShadowLevel) {
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 1
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = level.radius
    }
}

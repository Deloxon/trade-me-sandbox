//
//  UICollectionViewFlowLayout+TradeMe.swift
//  Trade Me Sandbox
//
//  Created by Jose Solorzano on 8/12/18.
//  Copyright © 2018 Jose Solorzano. All rights reserved.
//

import Foundation
import UIKit

extension UICollectionViewFlowLayout {
    public var columns : CGFloat { return 3 }
    public var portraitHeight : CGFloat { return 100 }
    public var padding : CGFloat { return 15 }
}

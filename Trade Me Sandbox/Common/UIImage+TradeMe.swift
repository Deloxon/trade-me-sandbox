//
//  UIImage+TradeMe.swift
//  Trade Me Sandbox
//
//  Created by Jose Solorzano on 8/11/18.
//  Copyright © 2018 Jose Solorzano. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    class var close : UIImage? {
        return UIImage(named: "closeIcon")
    }
}

//
//  UIColor+TradeMe.swift
//  Trade Me Sandbox
//
//  Created by Jose Solorzano on 8/11/18.
//  Copyright © 2018 Jose Solorzano. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    class var primaryColor : UIColor {
        return UIColor(red: 251 / 255.0, green: 204 / 255.0 , blue: 72.0 / 255.0, alpha: 1.0)
    }
    
    class var secondaryColor : UIColor {
        return UIColor(red: 22.0 / 255.0, green: 121.0 / 255.0 , blue: 219.0 / 255.0, alpha: 1.0)
    }
    
    class var separatorColor : UIColor {
        return UIColor.lightGray.withAlphaComponent(0.5)
    }
    
    class var lightTradeMeText : UIColor {
        return UIColor.lightGray
    }
    
    class var darkTradeMeText : UIColor {
        return UIColor.black
    }
    
    class var greenText : UIColor {
        return UIColor.green
    }
    
    class var unselectedPageControl : UIColor {
        return UIColor.black.withAlphaComponent(0.3)
    }
    
    class var selectedPageControl : UIColor {
        return UIColor.black.withAlphaComponent(0.8)
    }
    
    class var transparentBackground : UIColor {
        return UIColor.black.withAlphaComponent(0.4)
    }
    
    class var solidBackground : UIColor {
        return UIColor.white
    }
    
    class var actionTitle : UIColor {
        return UIColor.white
    }
    
    class var dirtyWhite : UIColor {
        return UIColor(red: 250.0 / 255.0, green: 250.0 / 255.0, blue: 250.0 / 255.0 , alpha: 1.0)
    }
}

//
//  UIApplication+TradeMe.swift
//  Trade Me Sandbox
//
//  Created by Jose Solorzano on 8/14/18.
//  Copyright © 2018 Jose Solorzano. All rights reserved.
//

import Foundation
import UIKit

extension UIApplication {
    
    /**
     Reads values from the info.PLIST file
    */
    static var apiKey: String? {
        return Bundle.main.object(forInfoDictionaryKey: "TradeMeApiKey") as? String
    }
    
    static var apiSecret: String? {
        return Bundle.main.object(forInfoDictionaryKey: "TradeMeApiSecret") as? String
    }
}

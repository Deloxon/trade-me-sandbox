//
//  FeatureFilteringViewModel.swift
//  Trade Me Sandbox
//
//  Created by Jose Solorzano on 8/9/18.
//  Copyright © 2018 Jose Solorzano. All rights reserved.
//

import Foundation
import RxSwift
import Moya
/**
 Enum for common errors while navigating categories
 */
enum CategoryNavigationError : Error {
    case noSubcategories
    case networkError
    case fatalError
    case decodingError
    
    /**
     A description to be used in UI
     */
    var friendlyDescrition : String {
        switch self {
        case .noSubcategories:
            return "This category does not have any subcategories."
        default:
            return "Unexpected network failure."
        }
    }
}

/**
 This class acts as a means of providing agnosticity between category navigation related
 view controllers and the data models, as well as
 isolating them from networking and business logic.
 */
class CategoryFilteringViewModel : ViewModel {
    
    ///MARK: Models
    private var rootCategory : Category
    public var categoryId : String? {
        return self.rootCategory.id
    }
    
    public var categoryName : String? {
        return self.rootCategory.name
    }
    
    ///MARK: UI drivers
    private var currentSubcategoriesPublisher : PublishSubject<[Category]> = PublishSubject<[Category]>()
    public var currentSubcategories : Observable<[Category]> {
        return currentSubcategoriesPublisher.asObserver()
    }
    
    private var errorPublisher : PublishSubject<Error> = PublishSubject<Error>()
    var errors : Observable<Error> {
        return errorPublisher.asObserver()
    }
    
    var shouldDisplayEmptyDataSet : Bool {
        if mainLoadingStatusVariable.value == .loading { return false }
        return self.hadNetworkError || self.rootCategory.id != ""
    }
    
    let categoryTitle = Variable<String>("")
    
    var emptyDataSetStringDescription : String {
        
        if hadNetworkError {
            return CategoryNavigationError.networkError.friendlyDescrition
        } else {
            return CategoryNavigationError.noSubcategories.friendlyDescrition
        }
    }
    
    ///MARL: rx
    let bag = DisposeBag()
    
    init(category : Category, provider : MoyaProvider<TradeMeSandbox>) {
        self.rootCategory = category
        self.categoryTitle.value = self.rootCategory.name
        super.init(provider: provider)
        self.errors.subscribe(onNext: { error in
            self.hadNetworkError = true
        }).disposed(by: bag)
    }
    
    ///MARK: Networking
    private var hadNetworkError : Bool = false
    
    /**
     Perform network reload
     */
    public func reload(forced : Bool = false) {
        self.hadNetworkError = false
        self.mainLoadingStatusVariable.value = .loading
        
        ///Won't perform a network request unless forced equals true or the category hasn't loaded
        guard ((self.rootCategory.id == "") || forced), !self.rootCategory.isLeaf  else {
            self.mainLoadingStatusVariable.value = .notLoading
            self.currentSubcategoriesPublisher.onNext(self.rootCategory.children ?? [])
            return
        }
        
        _ = networkProvider.rx.request(.categories(inCategoryId: self.rootCategory.id)).subscribe { event in
            self.mainLoadingStatusVariable.value = .notLoading
            switch event {
            case .success(let response):
                do {
                    let decoder = JSONDecoder()
                    let node = try decoder.decode(Category.self, from: response.data)
                    self.currentSubcategoriesPublisher.onNext(node.children ?? [])
                    
                } catch(_) {
                    self.errorPublisher.onNext(CategoryNavigationError.decodingError)
                    self.currentSubcategoriesPublisher.onNext([])
                }
            case .error(_):
                self.errorPublisher.onNext(CategoryNavigationError.networkError)
                self.currentSubcategoriesPublisher.onNext([])
            }
        }
    }
}

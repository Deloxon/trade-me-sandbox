//
//  CategoryListViewController
//  Trade Me Sandbox
//
//  Created by Jose Solorzano on 8/9/18.
//  Copyright © 2018 Jose Solorzano. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import RxSwift
import RxCocoa
import DZNEmptyDataSet

/**
 This view controller uses a table view as a list of categories,
 users can tap on different categories to navigate them.
 
 In case of a network failure, the empty data set pattern is used to display friendly user messages.
 
 Such pattern is also used for leaf categories.
 */
class CategoryListViewController : UIViewController {
    
    public var viewModel : CategoryFilteringViewModel?
    public var navigationDelegate : CategoryNavigationDelegate?
    public var interactive : Bool = false
    public var tableView : UITableView = UITableView()
    private let cellIdentifier = "CategoryCell"
    private let bag = DisposeBag()
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControlEvents.valueChanged)
        return refreshControl
    }()
    
    @objc public func refresh() {
        self.viewModel?.reload(forced : true )
        self.tableView.reloadData()
    }
    
    convenience init() {
        self.init(nibName: nil, bundle: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTableView()
        self.setupBindings()
        self.setupCellTapHandling()
        if interactive { self.registerBarButtonItems() }
        self.viewModel?.reload()
    }
    
    private func setupTableView() {
        self.view.addSubview(tableView)
        tableView.snp.makeConstraints { (maker) in
            maker.top.equalTo(self.view)
            maker.right.equalTo(self.view)
            maker.bottom.equalTo(self.view)
            maker.left.equalTo(self.view)
        }
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: self.cellIdentifier)
        tableView.tableFooterView = UIView()
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        tableView.addSubview(refreshControl)
    }
    
    private func registerBarButtonItems() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Apply", style: .done, target: self, action: #selector(dismissFiltering))
    }
    
    @objc private func dismissFiltering() {
        self.navigationDelegate?.hideFilters()
    }
    
    ///Setup RX bindings
    private func setupBindings() {
        
        guard let viewModel = self.viewModel else { return }
        
        viewModel.currentSubcategories.bind(to:
            tableView.rx.items(cellIdentifier: self.cellIdentifier,
                               cellType: UITableViewCell.self)) {  _, category, cell in
                                cell.textLabel?.text = "\(category.name)"
            }.disposed(by: bag)
        
        viewModel.currentSubcategories.subscribe(onNext: { _ in
            self.tableView.reloadData()
        }).disposed(by: bag)
        
        viewModel.mainLoadingStatus.subscribe(onNext: { status in
            if status == .loading {
                self.refreshControl.isHidden = false
                self.refreshControl.beginRefreshing()
            } else {
                self.refreshControl.isHidden = true
                self.refreshControl.endRefreshing()
            }
        }).disposed(by: bag)
        
        viewModel.categoryTitle.asDriver()
            .drive(self.navigationItem.rx.title)
            .disposed(by: bag)
    }
    
    ///Setup cell tapping
    func setupCellTapHandling() {
        tableView.rx.modelSelected(Category.self).subscribe(onNext: { category in
            
            self.navigationDelegate?.did(choose: category)
            if let selectedRowIndexPath = self.tableView.indexPathForSelectedRow {
                self.tableView.deselectRow(at: selectedRowIndexPath, animated: true)
            }
            
        }).disposed(by: bag)
    }
}

///MARK: Empty data set data source
extension CategoryListViewController : DZNEmptyDataSetSource {
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        return NSAttributedString(string: "Oops!")
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString {
        return NSAttributedString(string: self.viewModel?.emptyDataSetStringDescription ?? "")
    }
}
///MARK: Empty data set delegate
extension CategoryListViewController : DZNEmptyDataSetDelegate {
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView) -> Bool {
        let shouldBeEmpty = self.viewModel?.shouldDisplayEmptyDataSet ?? true
        return shouldBeEmpty
    }
    
    func emptyDataSet(_ scrollView: UIScrollView!, didTap view: UIView!) {
        self.refresh()
    }
}

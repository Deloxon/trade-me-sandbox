//
//  Category.swift
//  Trade Me Sandbox
//
//  Created by Jose Solorzano on 8/9/18.
//  Copyright © 2018 Jose Solorzano. All rights reserved.
//

import Foundation

/**
 Category data model, this is an example of an use case where a class is required over a struct.
 Structs can't store properties of the same type.
 */
class Category : Codable {
    
    weak var parent : Category?
    var name : String = ""
    var id : String? = ""
    var children : [Category]? = []
    var isLeaf : Bool = false
    
    init(id : String? = nil, name : String) {
        self.id = id
        self.name = name
    }
    
    enum CodingKeys: String, CodingKey
    {
        case name = "Name"
        case id = "Number"
        case children = "Subcategories"
        case isLeaf = "IsLeaf"
    }
    
    ///We provide a custom decoding algo to be able to inject a parent into each node
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let id = try? container.decode(String.self, forKey: .id)
        let name = try container.decode(String.self, forKey: .name)
        let children = try? container.decode([Category].self, forKey: .children)
        let isLeaf = try container.decode(Bool.self, forKey: .isLeaf)
        
        self.id = id
        self.name = name
        self.children = children
        self.isLeaf = isLeaf
        
        for child in self.children ?? [] {
            child.parent = self
        }
    }
}

/**
 A means to initially provide the name of the root category before loeading
 */
extension Category {
    static func root() -> Category {
        return Category(id : "", name: "All categories")
    }
    
    static func empty() -> Category {
        return Category(id : "empty", name: "All categories")
    }
}


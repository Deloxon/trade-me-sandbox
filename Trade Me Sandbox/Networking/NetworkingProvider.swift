//
//  NetworkingProvider.swift
//  Trade Me Sandbox
//
//  Created by Jose Solorzano on 8/9/18.
//  Copyright © 2018 Jose Solorzano. All rights reserved.
//

import Foundation
import Moya

/// Enum for `some` of the photo sizes in the API
enum PhotoSize : String {
    case thumbnail = "Thumbnail",
    list = "List",
    medium = "Medium"
}

/// Base enum on top of which the API target is built
enum TradeMeSandbox {
    /**
     Endpoint that provides categories for a given root id
     - Parameters:
     - categoryId: ID of the root category
     */
    case categories(inCategoryId : String?)
    /**
     Endpoint that provides listings in a given category
     - Parameters:
     - categoryId: ID of the category
     - photoSize: size of the photos to be included where applicable
     */
    case listings(inCategoryId : String, photoSize : PhotoSize)
    /**
     Endpoint that provides details of a given listing
     - Parameters:
     - Id: ID of the listing
     */
    case listingDetails(forId : Int)
}

/// Conformation to the TargetType protocol
extension TradeMeSandbox : TargetType {
    var baseURL: URL { return URL(string: "https://api.tmsandbox.co.nz/v1/")! }
    
    var path: String {
        switch self {
        case .categories(let id):
            if let unwrappedId = id, unwrappedId != "" {
                return "Categories/\(unwrappedId).json"
            } else {
                return "Categories.json"
            }
        case .listings(_,_):
            return "/Search/General.json"
        case .listingDetails(let id):
            return "/Listings/\(id).json"
        }
    }
    
    var method: Moya.Method {
        return .get
    }
    
    var task: Task {
        switch self {
        case .listingDetails(_), .categories(_):
            return .requestPlain
        case let .listings(category, size):
            return .requestParameters(parameters: ["category": category, "photo_size" : size.rawValue], encoding: URLEncoding.queryString)
        }
    }
    
    var sampleData: Data {
        switch self{
        case .categories(_):
            return NetworkingManager.sharedInstance.stubbedResponse("categories")
        case .listingDetails(_):
            return NetworkingManager.sharedInstance.stubbedResponse("details")
        case .listings(let id,_):
            if id == "empty" {
                return NetworkingManager.sharedInstance.stubbedResponse("emptylistings")
            } else {
                return NetworkingManager.sharedInstance.stubbedResponse("listings")
            }
        }
    }
    
    var headers: [String: String]? {
        return [:]
    }
}

/// Custom plugin to inject OAuth header information
class TradeMeOAuthPlugin: PluginType {
    private var key : String
    private var secret : String
    init(withKey key : String, secret : String ) {
        self.key = key
        self.secret = secret
    }
    
    func prepare(_ request: URLRequest, target: TargetType) -> URLRequest {
        var request = request
        request.addValue("OAuth oauth_consumer_key=\(key)", forHTTPHeaderField: "Authorization")
        request.addValue("oauth_signature_method=\"PLAINTEXT\"", forHTTPHeaderField: "Authorization")
        request.addValue("oauth_signature=\(secret)&", forHTTPHeaderField: "Authorization")
        return request
    }
}

/// This class will provide a singleton `provider` that can be used by middlemen and view models throughout the trade me app
class NetworkingManager : NSObject {
    
    static var sharedInstance = NetworkingManager()
    public var provider : MoyaProvider<TradeMeSandbox>
    public var bundle : Bundle = Bundle.main
    
    private let authPlugin = TradeMeOAuthPlugin(withKey: UIApplication.apiKey ?? "", secret: UIApplication.apiSecret ?? "")
    override private init() {
        let loggingPlugin = NetworkLoggerPlugin(verbose: true, cURL: true)
        self.provider = MoyaProvider(stubClosure : MoyaProvider.neverStub, plugins: [ authPlugin, loggingPlugin])
    }
    
    func stubbedResponse(_ filename: String) -> Data! {
        let bundle = self.bundle
        let path = bundle.path(forResource: filename, ofType: "json")
        return (try? Data(contentsOf: URL(fileURLWithPath: path!)))
    }
}

// MARK: - Support methods
/**
 Reads a JSON file from the project directory
 
 - Parameters:
 - filename: filename without an extension
 - Returns: Raw data to be interpreted by decoding methods.
 */

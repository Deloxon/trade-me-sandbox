//
//  App.swift
//  Trade Me Sandbox
//
//  Created by Jose Solorzano on 8/9/18.
//  Copyright © 2018 Jose Solorzano. All rights reserved.
//

import Foundation
import UIKit
import DeviceKit

/**
 Coordinator protocol with a single start method
 */
protocol Coordinator : class {
    ///Starts the initial flow for confirming coordinators
    func start()
}

/**
 First coordinator in the regular app use case, sets a Navigation controller as root for the application flow.
 - Parameters:
 - window: Window object provided by the app delegate
 */
class TradeMeSandboxCoordinator : Coordinator {
    
    private var window : UIWindow
    private let rootViewController : UINavigationController
    private let initialCoordinator : Coordinator
    private let device = Device()
    
    init(window : UIWindow) {
        self.window = window
        let blankNavigationController = CategoryNavigationController(nibName: nil, bundle: nil)
        blankNavigationController.isNavigationBarHidden = device.isPad
        self.rootViewController = blankNavigationController
        self.initialCoordinator = CategoriesNavigationCoordinator(presenter: self.rootViewController)
    }
    
    func start() {
        window.rootViewController = rootViewController
        self.initialCoordinator.start()
        window.makeKeyAndVisible()
    }
}

/**
 Coordinator class that presents the listings in given categories, it is in charge of launching an iPad or iPhone layout and navigating through categories.
 
 ///Note: iPad and iPhone use all the same classes, the only difference is an additional split view controller for the iPad
 
 - Parameters:
 - presenter: Navigation Controller to be pushed into
 */
class CategoriesNavigationCoordinator : NSObject, Coordinator {
    
    private let categoriesNavigationViewController : UINavigationController
    private let listingsViewController : ListingsViewController
    private let presenter : UINavigationController
    private let device = Device()
    
    init(presenter : UINavigationController) {
        self.presenter = presenter
        
        let categoryListViewController = CategoryListViewController()
        categoryListViewController.viewModel = CategoryFilteringViewModel(category: Category.root(), provider: NetworkingManager.sharedInstance.provider)
        categoryListViewController.interactive = device.isPhone
        self.categoriesNavigationViewController = CategoryNavigationController(rootViewController: categoryListViewController)
        self.listingsViewController = ListingsViewController()
        self.listingsViewController.viewModel = ListingsViewModel(provider: NetworkingManager.sharedInstance.provider)
        self.listingsViewController.interactive = device.isPhone
    }
    
    func start() {
        if let categoryListViewController = self.categoriesNavigationViewController.viewControllers.first as? CategoryListViewController {
            categoryListViewController.navigationDelegate = self
        }
        
        self.listingsViewController.navigationDelegate = self
        self.categoriesNavigationViewController.delegate = self
        
        if device.isPad {
            let splitViewController = ManuallySplitViewController(master: categoriesNavigationViewController, detail: listingsViewController)
            presenter.pushViewController(splitViewController, animated: false)
        } else {
            self.presenter.pushViewController(listingsViewController, animated: true)
        }
    }
}

/**
 Extends the categories navigation coordinator to actively listen to modifications in the navigation stack
 */
extension CategoriesNavigationCoordinator : UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        if let categoryViewController = viewController as? CategoryListViewController,
            let viewModel = categoryViewController.viewModel, let categoryId = viewModel.categoryId, let categoryName = viewModel.categoryName {
            
            ///Render the selected category listings
            self.listingsViewController.viewModel?.category = Category(id: categoryId, name : categoryName)
        }
    }
}

/**
 Delegate class the coordinator conforms to, in order to provide a means of communication from other classes such as the View Models or the View Controllers. In this basic setup the View Controllers are used.
 
 This protocol is the main driver for user navigation and interaction.
 */
protocol CategoryNavigationDelegate : class {
    func did(choose : Category)
    func showFilters()
    func hideFilters()
    func display(_ listing : Listing)
}

/**
 Conformance to the navigation delegate protocol.
 */
extension CategoriesNavigationCoordinator : CategoryNavigationDelegate {
    
    ///Hides a modally presented categories list
    func hideFilters() {
        self.categoriesNavigationViewController.dismiss(animated: true, completion: nil)
    }
    
    ///Modally presents a list of categories
    func showFilters() {
        self.presenter.present(self.categoriesNavigationViewController, animated: true, completion: nil)
    }
    
    /**
     Modally presents the details of a listing
     - Parameters:
     - listing: Listing object to be used by the view model initializer
     */
    func display(_ listing : Listing) {
        let detailsViewController = ListingDetailViewController()
        detailsViewController.modalPresentationStyle = .overCurrentContext
        detailsViewController.modalTransitionStyle = device.isPhone ? .coverVertical : .crossDissolve
        detailsViewController.largeLayout = self.device.isPad
        detailsViewController.viewModel = ListingDetailViewModel(listing: listing, provider : NetworkingManager.sharedInstance.provider)
        self.presenter.present(detailsViewController, animated: true, completion: nil)
    }
    
    /**
     Pushes a category list view controller into the navigation stack
     - Parameters:
     - category: Category object to be used by the view model initializer
     */
    func did(choose category : Category) {
        let categoryListViewController = CategoryListViewController()
        categoryListViewController.navigationDelegate = self
        categoryListViewController.interactive = device.isPhone
        categoryListViewController.viewModel = CategoryFilteringViewModel(category: category, provider : NetworkingManager.sharedInstance.provider)
        self.categoriesNavigationViewController.pushViewController(categoryListViewController, animated: true)
    }
}




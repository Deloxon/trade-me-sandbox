//
//  PhotoSliderCell.swift
//  Trade Me Sandbox
//
//  Created by Jose Solorzano on 8/12/18.
//  Copyright © 2018 Jose Solorzano. All rights reserved.
//

import Foundation
import FSPagerView
import UIKit
import SnapKit

/**
 Subclass of the FSPagerViewCell in order to add the loading spinner
 */
class TradeMePagerViewCell : FSPagerViewCell {
    
    private lazy var activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupActivityIndicator()
    }
    
    private func setupActivityIndicator() {
        activityIndicator.hidesWhenStopped = true
        self.contentView.addSubview(activityIndicator)
        activityIndicator.snp.makeConstraints { (maker) in
            maker.center.equalTo(self.contentView)
        }
    }
    /**
     Sets the image from a URL through alamofire image and displays an activity indicator while it loads.
     - Parameters:
     - image: URL of the image to be displayed
     */
    public func set(image : URL) {
        self.activityIndicator.startAnimating()
        self.imageView?.af_setImage(withURL: image, placeholderImage: nil, filter: nil, imageTransition: .crossDissolve(0.5), runImageTransitionIfCached: false, completion: { response in
            self.activityIndicator.stopAnimating()
        })
    }
    
    override func prepareForReuse() {
        self.activityIndicator.stopAnimating()
        self.imageView?.image = nil
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

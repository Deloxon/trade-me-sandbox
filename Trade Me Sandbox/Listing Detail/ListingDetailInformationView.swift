//
//  ListingDetailInformationView.swift
//  Trade Me Sandbox
//
//  Created by Jose Solorzano on 8/12/18.
//  Copyright © 2018 Jose Solorzano. All rights reserved.
//

import Foundation
import UIKit
/**
 Abstraction class to display the information of a listing.
 */
class ListingDetailInformationView: UIView {
    
    lazy var titleLabel = UILabel()
    lazy var priceLabel = UILabel()
    lazy var reserveLabel = UILabel()
    lazy var actionButton = UIButton()
    lazy var timeLabel = UILabel()

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupViews()
    }
    
    /**
     Setup and configure subviews
     */
    private func setupViews() {
        let normalPadding = 8
        let largePadding = 20
        let actionHeight = 50
        
        self.addSubview(titleLabel)
        titleLabel.font = UIFont.detailTitleFont()
        titleLabel.text = "El titulo"
        titleLabel.textAlignment = .center
        titleLabel.snp.makeConstraints { (maker) in
            maker.top.equalTo(self).offset(normalPadding)
            maker.left.equalTo(self)
            maker.right.equalTo(self)
        }
        
        self.addSubview(priceLabel)
        priceLabel.font = UIFont.detailPriceFont()
        priceLabel.text = "$444.4"
        priceLabel.textAlignment = .center
        priceLabel.snp.makeConstraints { (maker) in
            maker.top.equalTo(titleLabel.snp.bottom).offset(largePadding)
            maker.left.equalTo(self)
            maker.right.equalTo(self)
        }
        
        self.addSubview(reserveLabel)
        reserveLabel.font = UIFont.mediumListingFont()
        reserveLabel.text = "No reserve"
        reserveLabel.textColor = UIColor.lightTradeMeText
        reserveLabel.textAlignment = .center
        reserveLabel.snp.makeConstraints { (maker) in
            maker.top.equalTo(priceLabel.snp.bottom)
            maker.left.equalTo(self)
            maker.right.equalTo(self)
        }
        
        self.addSubview(actionButton)
        actionButton.setBackgroundColor(color: UIColor.secondaryColor, forState: .normal)
        actionButton.setTitleColor(UIColor.actionTitle, for: .normal)
        actionButton.layer.cornerRadius = 5
        actionButton.clipsToBounds = true
        actionButton.setTitle("Action", for: .normal)
        actionButton.snp.makeConstraints { (maker) in
            maker.top.equalTo(reserveLabel.snp.bottom).offset(normalPadding)
            maker.left.equalTo(self).offset(largePadding)
            maker.right.equalTo(self).offset(-largePadding)
            maker.height.equalTo(actionHeight)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//
//  ListingDetailViewModel.swift
//  Trade Me Sandbox
//
//  Created by Jose Solorzano on 8/11/18.
//  Copyright © 2018 Jose Solorzano. All rights reserved.
//

import Foundation
import RxSwift
import Moya
/**
 This class acts as a means of providing agnosticity between the
 detail navigation controller and the data model, as well as
 isolating it from networking and business logic.
 */
class ListingDetailViewModel : ViewModel {
    
    /**
     Trigger a signal to the RX sequences when the detailed listing is set
     */
    private var baseListing : Listing
    private var detailedListing : DetailedListing? {
        didSet {
            if let listing = detailedListing {
                self.listingPhotos.value = listing.photos
                self.title.value = listing.title
                self.displayPrice.value = listing.displayPrice
                self.reserveText.value = listing.reserveState.friendlyDescription
            }
        }
    }
    
    ///MARK: UI drivers
    private var errorPublisher : PublishSubject<Error> = PublishSubject<Error>()
    var errors : Observable<Error> {
        return errorPublisher.asObserver()
    }
    
    var shouldDisplayEmptyDataSet : Bool {
        if mainLoadingStatusVariable.value == .loading { return false }
        return self.hadNetworkError
    }
    public let listingPhotos = Variable<[URL]>([])
    public let title = Variable<String>("")
    public let displayPrice = Variable<String>("")
    public let reserveText = Variable<String>("")
    
    var emptyDataSetStringDescription : String {
        
        if hadNetworkError {
            return CategoryNavigationError.networkError.friendlyDescrition
        } else {
            return CategoryNavigationError.noSubcategories.friendlyDescrition
        }
    }
    
    ///Mark: rx
    let bag = DisposeBag()
    
    init(listing : Listing, provider : MoyaProvider<TradeMeSandbox>) {
        self.baseListing = listing
        super.init(provider : provider)
        self.errors.subscribe(onNext: { error in
            self.hadNetworkError = true
        }).disposed(by: bag)
    }
    
    ///Mark: networking
    private var hadNetworkError : Bool = false
    
    /**
      Reload network
     */
    public func reload() {
        self.hadNetworkError = false
        self.mainLoadingStatusVariable.value = .loading
        
        _ = networkProvider.rx.request(.listingDetails(forId: baseListing.id)).subscribe { event in
            self.mainLoadingStatusVariable.value = .notLoading
            switch event {
            case .success(let response):
                do {
                    let decoder = JSONDecoder()
                    let listing = try decoder.decode(DetailedListing.self, from: response.data)
                    self.detailedListing = listing
                    
                } catch(let error) {
                    print(error)
                    self.errorPublisher.onNext(CategoryNavigationError.decodingError)
                }
            case .error(_):
                self.errorPublisher.onNext(CategoryNavigationError.networkError)
            }
        }
    }
}

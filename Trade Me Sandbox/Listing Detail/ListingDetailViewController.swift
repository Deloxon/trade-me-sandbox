//
//  ListingDetailViewController.swift
//  Trade Me Sandbox
//
//  Created by Jose Solorzano on 8/11/18.
//  Copyright © 2018 Jose Solorzano. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import FSPagerView
import RxSwift

/**
 This view controller displays the details of a given listing
 */
///TODO: Lots of things that require further knowledge of the trade me business logic
class ListingDetailViewController : UIViewController {
    
    ///Base layout
    public var largeLayout : Bool = false
    private lazy var solidBackground = UIView()
    
    ///Photo paging
    public lazy var pager = FSPagerView()
    private lazy var pageControl = FSPageControl()
    
    ///View model
    public var viewModel : ListingDetailViewModel?
    private let bag = DisposeBag()
    
    ///Information
    public lazy var informationView = ListingDetailInformationView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViews()
        self.setupBindings()
        self.viewModel?.reload()
    }
    
    private func setupBindings() {
        guard let viewModel = self.viewModel else { return }
        
        viewModel.listingPhotos.asObservable().subscribe(onNext: { photos in
            self.pager.reloadData()
            self.pageControl.numberOfPages = photos.count
        }).disposed(by: bag)
        
        viewModel.title.asObservable().bind(to: self.informationView.titleLabel.rx.text).disposed(by: bag)
        viewModel.reserveText.asObservable().bind(to: self.informationView.reserveLabel.rx.text).disposed(by: bag)
        viewModel.displayPrice.asObservable().bind(to: self.informationView.priceLabel.rx.text).disposed(by: bag)
    }
    
    /**
      Configure my views and my constraints
      Large method suggests subclasses should be created.
     */
    private func setupViews() {
        let largePadding = 20
        let sidesPadding = 100
        let pageControlPadding : CGFloat = 20
        let pageControlHeight = 30
        
        self.view.backgroundColor = UIColor.transparentBackground
        solidBackground.backgroundColor = UIColor.dirtyWhite
        self.view.addSubview(solidBackground)
        solidBackground.snp.remakeConstraints { (maker) in
            maker.top.equalTo(self.view)
            maker.bottom.equalTo(self.view)
            maker.left.equalTo(self.view).offset(largeLayout ? sidesPadding : 0 )
            maker.right.equalTo(self.view).offset(largeLayout ? -sidesPadding : 0 )
        }
        
        ///Photo paging
        self.solidBackground.addSubview(pager)
        pager.register(TradeMePagerViewCell.self, forCellWithReuseIdentifier: "photo")
        pager.dataSource = self
        pager.delegate = self
        pager.snp.makeConstraints  { (maker) in
            maker.right.equalTo(self.solidBackground)
            maker.left.equalTo(self.solidBackground)
            maker.top.equalTo(self.solidBackground)
            maker.height.equalTo(self.solidBackground.snp.width).multipliedBy(0.75)
        }
        
        self.solidBackground.addSubview(pageControl)
        pageControl.contentHorizontalAlignment = .center
        pageControl.setFillColor(UIColor.unselectedPageControl, for: .normal)
        pageControl.setStrokeColor(UIColor.unselectedPageControl, for: .normal)
        pageControl.setStrokeColor(UIColor.selectedPageControl, for: .selected)
        pageControl.setFillColor(UIColor.selectedPageControl, for: .selected)
        pageControl.contentInsets = UIEdgeInsets(top: 0, left: pageControlPadding, bottom: 0, right: pageControlPadding)
        pageControl.snp.makeConstraints  { (maker) in
            maker.right.equalTo(self.solidBackground)
            maker.left.equalTo(self.solidBackground)
            maker.top.equalTo(self.pager.snp.bottom)
            maker.height.equalTo(pageControlHeight)
        }
        
        ///Close button
        let closeButton = UIButton()
        closeButton.setBackgroundImage(UIImage.close, for: .normal)
        self.solidBackground.addSubview(closeButton)
        closeButton.addTarget(self, action: #selector(close), for: .touchUpInside)
        closeButton.snp.makeConstraints { (maker) in
            maker.right.equalTo(self.solidBackground).offset(-largePadding)
            maker.top.equalTo(self.solidBackground).offset(largePadding)
            maker.size.equalTo(40)
        }
        
        ///Main information
        self.solidBackground.addSubview(informationView)
        informationView.snp.makeConstraints { (maker) in
            maker.top.equalTo(self.pageControl.snp.bottom)
            maker.left.equalTo(self.solidBackground)
            maker.right.equalTo(self.solidBackground)
            maker.bottom.equalTo(self.solidBackground)
        }
    }
    
    @objc private func close() {
        ///FIXME: Delegate should close me, not myself.
        self.dismiss(animated: true)
    }
}

///MARK: Pager delegate and data source
extension ListingDetailViewController : FSPagerViewDataSource {
    public func numberOfItems(in pagerView: FSPagerView) -> Int {
        return self.viewModel?.listingPhotos.value.count ?? 0
    }
    
    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "photo", at: index)
        
        if let photoURL = self.viewModel?.listingPhotos.value[index], let typedCell = cell as? TradeMePagerViewCell {
            typedCell.set(image: photoURL)
        }
        
        return cell
    }
}

extension ListingDetailViewController : FSPagerViewDelegate {
    func pagerViewDidScroll(_ pagerView: FSPagerView) {
        self.pageControl.currentPage = pagerView.currentIndex
    }
}


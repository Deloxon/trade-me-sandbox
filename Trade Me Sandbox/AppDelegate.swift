//
//  AppDelegate.swift
//  Trade Me Sandbox
//
//  Created by Jose Solorzano on 8/9/18.
//  Copyright © 2018 Jose Solorzano. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var coordinator : Coordinator?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        self.setupApp()
        return true
    }
    
    /**
     An initial coordinator for the app flow is created, assigned and started.
     */
    private func setupApp() {
        let window = UIWindow(frame: UIScreen.main.bounds)
        let tradeMeCoordinator = TradeMeSandboxCoordinator(window: window)
        
        self.window = window
        self.coordinator = tradeMeCoordinator
        
        tradeMeCoordinator.start()
    }
}


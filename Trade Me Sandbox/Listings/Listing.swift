//
//  Listing.swift
//  Trade Me Sandbox
//
//  Created by Jose Solorzano on 8/11/18.
//  Copyright © 2018 Jose Solorzano. All rights reserved.
//

import Foundation

/**
 Data model for the Listing object.
 
 A class is used for purposes of inheritance.
 */
class Listing : Codable {
    var id : Int
    ///Titling information
    var region : String = ""
    var title : String = ""
    var hasFreeShipping : Bool = false
    ///Buy now
    var hasBuyNow : Bool = false
    var buyNowPrice : Float = 0.0
    ///Reserve
    var reserveState : ListingReserveState = .none
    var displayPrice : String = ""
    ///Photos
    var thumbnail : URL?
    
    init(id : Int) {
        self.id = id
        self.title = ""
    }
    
    static func fake() -> Listing {
        return Listing(id: 0)
    }
    
    enum CodingKeys: String, CodingKey
    {
        case id = "ListingId"
        case region = "Region"
        case title = "Title"
        case hasFreeShipping = "HasFreeShipping"
        case hasBuyNow = "HasBuyNow"
        case reserveState = "ReserveState"
        case buyNowPrice = "BuyNowPrice"
        case displayPrice = "PriceDisplay"
        case thumbnail = "PictureHref"
    }
    
    //We use a custom algo to be able to parse optional API fields into non optional class properties
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let id = try container.decode(Int.self, forKey: .id)
        let region = try container.decode(String.self, forKey: .region)
        let title = try container.decode(String.self, forKey: .title)
        let hasFreeShipping : Bool? = try? container.decode(Bool.self, forKey: .hasFreeShipping)
        let hasBuyNow : Bool? = try? container.decode(Bool.self, forKey: .hasBuyNow)
        let buyNowPrice : Float? = try? container.decode(Float.self, forKey: .buyNowPrice)
        let reserveState : ListingReserveState? = try? container.decode(ListingReserveState.self, forKey: .reserveState)
        let displayPrice : String? = try? container.decode(String.self, forKey: .displayPrice)
        let thumbnail : URL? = try? container.decode(URL.self, forKey: .thumbnail)

        
        self.id = id
        self.region = region
        self.title = title
        self.hasFreeShipping = hasFreeShipping ?? false
        self.hasBuyNow = hasBuyNow ?? false
        self.buyNowPrice = buyNowPrice ?? 0.0
        self.reserveState = reserveState ?? .notApplicable
        self.displayPrice = displayPrice ?? ""
        self.thumbnail = thumbnail
    }
}

class DetailedListing : Listing {
    var photos : [URL] = []

    private enum DetailedCodingKeys: String, CodingKey {
        case photos = "Photos"
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: DetailedCodingKeys.self)
        let urlStrings = try container.decode([Photo].self, forKey: .photos)
        self.photos = urlStrings.map { URL.from($0) }
        try super.init(from: decoder)
    }
}

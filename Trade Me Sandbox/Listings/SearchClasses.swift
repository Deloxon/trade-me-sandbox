//
//  SearchClasses.swift
//  Trade Me Sandbox
//
//  Created by Jose Solorzano on 8/12/18.
//  Copyright © 2018 Jose Solorzano. All rights reserved.
//

import Foundation


/**
 Root abstractor for the listings search results
 */
struct SearchResults : Codable {
    var listings : [Listing] = []
    
    enum CodingKeys: String, CodingKey
    {
        case listings = "List"
    }
}

/**
 Abstractor for photos of a listing
 */
struct Photo : Codable {
    var key : Int
    
    enum CodingKeys: String, CodingKey
    {
        case key = "Key"
    }
}

/**
 Enum for the reserve state of a listing
 */
enum ListingReserveState : Int, Codable {
    case none = 0
    case met = 1
    case notMet = 2
    case notApplicable = 3
    
    var shouldDisplayReserveInformation : Bool {
        switch self {
        case .notApplicable:
            return false
        default:
            return true
        }
    }
    
    ///Message for the UI
    var friendlyDescription : String {
        switch self {
        case .met:
            return "Reserve met"
        case .notApplicable:
            return ""
        case .none:
            return "No reserve"
        case .notMet:
            return "Reserve not met"
        }
    }
}

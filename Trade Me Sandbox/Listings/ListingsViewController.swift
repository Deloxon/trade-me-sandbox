//
//  ListingsViewController.swift
//  Trade Me Sandbox
//
//  Created by Jose Solorzano on 8/9/18.
//  Copyright © 2018 Jose Solorzano. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import RxSwift
import RxCocoa
import DeviceKit
import DZNEmptyDataSet

/**
 This view controller uses a collection view to display listings
 users can tap on listings to view their details.
 
 In case of a network failure, the empty data set pattern is used to display friendly user messages.
 */
class ListingsViewController : UIViewController {
    
    public var navigationDelegate : CategoryNavigationDelegate?
    public var interactive : Bool = false
    private lazy var filterButton : UIButton = UIButton()
    private let device = Device()
    
    ///Collection
    public var collectionView : UICollectionView!
    private let layout = UICollectionViewFlowLayout()
    private let squareIdentifier = "SquareListingCell"
    private let horizontalIdentifier = "ListingCell"
    
    ///Rx
    private let bag = DisposeBag()
    
    ///View model
    public var viewModel : ListingsViewModel?
    
    ///Pull to refresh
    lazy var refreshControl: UIRefreshControl = {
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControlEvents.valueChanged)
        
        return refreshControl
    }()
    
    @objc public func refresh() {
        self.viewModel?.reload(forced : true )
        self.collectionView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupCollectionView()
        self.setupCollectionViewBinding()
        self.setupCellTapHandling()
        if interactive {
            self.setupFilteringViews()
        }
    }
    
    convenience init() {
        self.init(nibName: nil, bundle: nil)
    }
    
    @objc private func filterButtonTapped() {
        self.navigationDelegate?.showFilters()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        self.collectionView.invalidateIntrinsicContentSize()
        self.collectionView.collectionViewLayout.invalidateLayout()
        self.collectionView.reloadData()
    }
}

///MARK: RX
/**
 Setup bindings and rx-related methods
 */
extension ListingsViewController {
    private func setupCollectionViewBinding() {
        guard let viewModel = self.viewModel else { return }
        
        viewModel.currentListings
            .bind(to: collectionView.rx.items) { (view, item, listing) in
                
                let currentOrientation = self.device.orientation
                let identifier : String = currentOrientation == .portrait ? self.horizontalIdentifier : self.squareIdentifier
                
                let cell = view.dequeueReusableCell(withReuseIdentifier: identifier, for: IndexPath(item: item, section: 0))
                
                if let typedCell = cell as? ListingsCollectionViewCell {
                    typedCell.configure(with: listing)
                }
                
                return cell
                
            }.disposed(by: bag)
        
        viewModel.mainLoadingStatus.subscribe(onNext: { status in
            if status == .loading {
                self.refreshControl.isHidden = false
                self.refreshControl.beginRefreshing()
            } else {
                self.refreshControl.isHidden = true
                self.refreshControl.endRefreshing()
            }
        }).disposed(by: bag)
        
        viewModel.categoryTitle.asDriver()
            .drive(self.navigationItem.rx.title)
            .disposed(by: bag)
    }
    
    func setupCellTapHandling() {
        collectionView.rx.modelSelected(Listing.self).subscribe(onNext: { listing in
            
            self.navigationDelegate?.display(listing)
            if let selectedPath = self.collectionView.indexPathsForSelectedItems?.first {
                self.collectionView.deselectItem(at: selectedPath, animated: true)
            }
            
        }).disposed(by: bag)
    }
}

///MARK: Layout delegate
/**
 For this use case, a delegate is used to avoid created a full, more complex subclass of a CollectionViewLayout
 */
extension ListingsViewController : UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let viewWidth = self.view.bounds.width
        
        if device.orientation == .portrait {
            return CGSize(width: viewWidth, height : layout.portraitHeight)
        } else {
            let side = (viewWidth / layout.columns) - layout.padding
            return CGSize(width: side,
                          height : side * 1.5)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return device.orientation == .portrait ? 0 : 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if device.orientation == .portrait {
            return UIEdgeInsetsMake(0,0,0,0)
        } else {
            return UIEdgeInsetsMake(0, 10, 0, 10)
        }
    }
}

///MARK: Setup views and constraints
extension ListingsViewController {
    private func setupFilteringViews() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Filter", style: .done, target: self, action: #selector(filterButtonTapped))
    }
    
    private func setupCollectionView() {
        self.collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        self.collectionView.backgroundColor = UIColor.white
        
        self.view.addSubview(collectionView)
        collectionView.snp.makeConstraints { (maker) in
            maker.top.equalTo(self.view)
            maker.right.equalTo(self.view)
            maker.bottom.equalTo(self.view)
            maker.left.equalTo(self.view)
        }
        collectionView.bounces = true
        collectionView.register(ListingsCollectionViewHorizontalCell.self, forCellWithReuseIdentifier: self.horizontalIdentifier)
        collectionView.register(ListingsCollectionViewSquareCell.self, forCellWithReuseIdentifier: self.squareIdentifier)
        collectionView.delegate = self
        collectionView.emptyDataSetSource = self
        collectionView.emptyDataSetDelegate = self
        collectionView.addSubview(refreshControl)
    }
}

///MARK: Empty data set pattern
extension ListingsViewController : DZNEmptyDataSetSource {
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        return NSAttributedString(string: "Oops!")
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString {
        return NSAttributedString(string: self.viewModel?.emptyDataSetStringDescription ?? "")
    }
}

extension ListingsViewController : DZNEmptyDataSetDelegate {
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView) -> Bool {
        let shouldBeEmpty = self.viewModel?.shouldDisplayEmptyDataSet ?? true
        return shouldBeEmpty
    }
    
    func emptyDataSet(_ scrollView: UIScrollView!, didTap view: UIView!) {
        guard let viewModel = self.viewModel else { return }
        if viewModel.isValidCategory {
            viewModel.reload(forced : true)
        }
    }
}

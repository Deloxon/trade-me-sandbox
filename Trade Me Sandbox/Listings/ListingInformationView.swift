//
//  ListingInformationView.swift
//  Trade Me Sandbox
//
//  Created by Jose Solorzano on 8/11/18.
//  Copyright © 2018 Jose Solorzano. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import AlamofireImage

/**
 UIView abstraction for the information of a listing inside a cell
 */
class ListingInformationView : UIView {
    
    private let padding : CGFloat = 8
    lazy var regionLabel = UILabel()
    lazy var freeShippingLabel = UILabel()
    lazy var titleLabel = UILabel()
    
    lazy var buyNowLabel = UILabel()
    lazy var buyNowPriceLabel = UILabel()
    
    lazy var displayPriceLabel = UILabel()
    lazy var reserveLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    ///Add subviews and create their constraints, as well as configuring them.
    private func setupViews() {
        self.addSubview(regionLabel)
        self.addSubview(freeShippingLabel)
        self.addSubview(titleLabel)
        self.addSubview(buyNowLabel)
        self.addSubview(buyNowPriceLabel)
        self.addSubview(displayPriceLabel)
        self.addSubview(reserveLabel)

        self.regionLabel.textColor = UIColor.lightTradeMeText
        self.regionLabel.font = UIFont.smallListingFont()
        self.regionLabel.snp.makeConstraints { (maker) in
            maker.left.equalTo(self).offset(padding)
            maker.top.equalTo(self).offset(padding)
        }
        
        self.freeShippingLabel.textColor = UIColor.greenText
        self.freeShippingLabel.text = "Free shipping"
        self.freeShippingLabel.font = UIFont.smallListingFont()
        self.freeShippingLabel.snp.makeConstraints { (maker) in
            maker.left.equalTo(self.regionLabel.snp.right).offset(padding)
            maker.top.equalTo(self).offset(padding)
        }
        
        self.titleLabel.textColor = UIColor.darkTradeMeText
        self.titleLabel.font = UIFont.mediumListingFont()
        self.titleLabel.numberOfLines = 2
        self.titleLabel.lineBreakMode = .byWordWrapping
        
        self.titleLabel.snp.makeConstraints { (maker) in
            maker.left.equalTo(self).offset(padding)
            maker.right.equalTo(self).offset(-padding)
            maker.top.equalTo(self.regionLabel.snp.bottom)
        }
        
        self.buyNowLabel.textColor = UIColor.lightTradeMeText
        self.buyNowLabel.font = UIFont.smallListingFont()
        self.buyNowLabel.text = "Buy Now"
        self.buyNowLabel.snp.makeConstraints { (maker) in
            maker.right.equalTo(self).offset(-padding)
            maker.bottom.equalTo(self).offset(-padding)
        }
        
        self.buyNowPriceLabel.textColor = UIColor.darkTradeMeText
        self.buyNowPriceLabel.font = UIFont.boldListingFont()
        self.buyNowPriceLabel.snp.makeConstraints { (maker) in
            maker.right.equalTo(self).offset(-padding)
            maker.bottom.equalTo(self.buyNowLabel.snp.top)
        }
        
        self.reserveLabel.textColor = UIColor.lightTradeMeText
        self.reserveLabel.font = UIFont.smallListingFont()
        self.reserveLabel.text = ""
        self.reserveLabel.snp.makeConstraints { (maker) in
            maker.left.equalTo(self).offset(padding)
            maker.bottom.equalTo(self).offset(-padding)
        }
        
        self.displayPriceLabel.textColor = UIColor.darkTradeMeText
        self.displayPriceLabel.font = UIFont.boldListingFont()
        self.displayPriceLabel.snp.makeConstraints { (maker) in
            maker.left.equalTo(self).offset(padding)
            maker.bottom.equalTo(self.reserveLabel.snp.top)
        }
    }
    
    /**
     This is a cleaning method to be consumed by the prepareForReuse of a cell
     */
    public func clean() {
        self.regionLabel.text = ""
        self.freeShippingLabel.isHidden = true
        self.buyNowLabel.isHidden = true
        self.buyNowPriceLabel.isHidden = true
        self.titleLabel.text = ""
        self.reserveLabel.text = ""
        self.displayPriceLabel.text = ""
    }
    
    /**
     Configure the view with a listing. Depending on the use case and size of a project, providing a model to a view
     is not always a great idea. Candidate for a FIXME.
     
     - Parameters:
     - listing: Listing object that provides the information to be displayed
     */
    public func configure(with listing : Listing) {
        
        self.regionLabel.text = listing.region
        self.freeShippingLabel.isHidden = !listing.hasFreeShipping
        
        self.buyNowLabel.isHidden = !listing.hasBuyNow
        self.buyNowPriceLabel.isHidden = !listing.hasBuyNow
        self.buyNowPriceLabel.text = "$\(listing.buyNowPrice)"
        
        self.titleLabel.text = listing.title.uppercased()
        self.reserveLabel.text = listing.reserveState.friendlyDescription
        self.displayPriceLabel.text = listing.displayPrice
    }
}

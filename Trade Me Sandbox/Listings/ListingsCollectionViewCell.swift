//
//  ListingsCollectionViewCell.swift
//  Trade Me Sandbox
//
//  Created by Jose Solorzano on 8/11/18.
//  Copyright © 2018 Jose Solorzano. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

///Layout type for listings cells
enum ListingsCollectionViewCellLayoutType {
    case horizontal,
    square
}

/**
 Base class for a listings collectiomn view cell, it provides the views and configuration methods.
 The subclass to be used will depend on the view controller and or view model.
 */
class ListingsCollectionViewCell : UICollectionViewCell{
    
    lazy var thumbnailView = UIImageView()
    lazy var informationView = ListingInformationView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    public func configure(with listing : Listing) {
        self.informationView.configure(with: listing)
        if let thumbnail = listing.thumbnail {
            self.thumbnailView.af_setImage(withURL: thumbnail)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        self.thumbnailView.image = nil
        self.informationView.clean()
    }
}

/**
 Squared layout cell subclass
 */
class ListingsCollectionViewSquareCell : ListingsCollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews() {
        self.layer.borderColor = UIColor.separatorColor.cgColor
        self.layer.borderWidth = 1
        
        self.contentView.addSubview(thumbnailView)
        thumbnailView.snp.makeConstraints { (maker) in
            maker.top.equalTo(self.contentView)
            maker.left.equalTo(self.contentView)
            maker.right.equalTo(self.contentView)
            maker.height.equalTo(self.contentView).multipliedBy(0.66)
        }
        
        self.contentView.addSubview(self.informationView)
        informationView.snp.makeConstraints { (maker) in
            maker.bottom.equalTo(self.contentView)
            maker.left.equalTo(self.contentView)
            maker.right.equalTo(self.contentView)
            maker.height.equalTo(self.contentView).multipliedBy(0.33)
        }
    }
}

/**
 Horizontal layout cell subclass
 */
class ListingsCollectionViewHorizontalCell : ListingsCollectionViewCell {
    
    lazy var separatorLine = UIView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews() {
        self.contentView.addSubview(separatorLine)
        separatorLine.backgroundColor = UIColor.separatorColor
        separatorLine.snp.makeConstraints { (maker) in
            maker.left.equalTo(self.contentView)
            maker.right.equalTo(self.contentView)
            maker.bottom.equalTo(self.contentView)
            maker.height.equalTo(1)
        }
        
        self.contentView.addSubview(self.informationView)
        informationView.snp.makeConstraints { (maker) in
            maker.bottom.equalTo(self.contentView)
            maker.top.equalTo(self.contentView)
            maker.right.equalTo(self.contentView)
            maker.left.equalTo(100)
        }
        
        self.contentView.addSubview(thumbnailView)
        thumbnailView.snp.makeConstraints { (maker) in
            maker.top.equalTo(self.contentView)
            maker.left.equalTo(self.contentView)
            maker.width.equalTo(100)
            maker.bottom.equalTo(self.contentView).offset(-1)
        }
    }
}

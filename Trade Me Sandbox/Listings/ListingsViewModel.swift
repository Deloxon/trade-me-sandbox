//
//  FeatureFilteringViewModel.swift
//  Trade Me Sandbox
//
//  Created by Jose Solorzano on 8/9/18.
//  Copyright © 2018 Jose Solorzano. All rights reserved.
//

import Foundation
import RxSwift
import Moya

/**
 Enum for common errors while navigating listings
 */
enum ListingError : Error {
    case noSearch,
    noListings
    
    var friendlyDescrition : String {
        switch self {
        case .noSearch:
            return "Filter categories to navigate our listings."
        default:
            return "This category doesn't have any listings."
        }
    }
}

/**
 This class acts as a means of providing agnosticity between listing navigation related
 view controllers and the data models, as well as
 isolating them from networking and business logic.
 */
class ListingsViewModel : ViewModel {
    
    ///MARK: Models
    public var category : Category? {
        didSet {
            self.reload(forced: true)
            if let title = self.category?.name {
                self.categoryTitle.value = title
            }
        }
    }
    
    ///MAR: UI drivers
    private var listingsPublsisher : PublishSubject<[Listing]> = PublishSubject<[Listing]>()
    public var currentListings : Observable<[Listing]> {
        return listingsPublsisher.asObserver()
    }
    
    private var errorPublisher : PublishSubject<Error> = PublishSubject<Error>()
    public var errors : Observable<Error> {
        return errorPublisher.asObserver()
    }
    
    public var shouldDisplayEmptyDataSet : Bool {
        if self.mainLoadingStatusVariable.value == .loading { return false }
        return true
    }
    
    public let categoryTitle = Variable<String>("Trade Me")
    public var isValidCategory : Bool {
        return self.category?.id != nil
    }
    
    public var emptyDataSetStringDescription : String {
        if hadNetworkError {
            return CategoryNavigationError.networkError.friendlyDescrition
        } else if self.category == nil {
            return ListingError.noSearch.friendlyDescrition
        } else {
            return ListingError.noListings.friendlyDescrition
        }
    }
    
    ///MARK: rx
    let bag = DisposeBag()
    
    override init(provider : MoyaProvider<TradeMeSandbox>) {
        super.init(provider: provider)
        self.errors.subscribe(onNext: { error in
            self.hadNetworkError = true
        }).disposed(by: bag)
    }
    
    ///MARK: Networking
    private var hadNetworkError : Bool = false
    
    /**
     Perform network reload
     */
    public func reload(forced : Bool = false) {
        self.hadNetworkError = false
        self.mainLoadingStatusVariable.value = .loading
        guard let categoryId = self.category?.id else { return }
        
        _ = networkProvider.rx.request(.listings(inCategoryId : categoryId, photoSize : .medium)).subscribe { event in
            self.mainLoadingStatusVariable.value = .notLoading
            switch event {
            case .success(let response):
                do {
                    let decoder = JSONDecoder()
                    let results = try decoder.decode(SearchResults.self, from: response.data)
                    self.listingsPublsisher.onNext(results.listings.limit())
                    
                } catch(let error) {
                    self.errorPublisher.onNext(CategoryNavigationError.decodingError)
                    self.listingsPublsisher.onNext([])
                }
            case .error(_):
                self.errorPublisher.onNext(CategoryNavigationError.networkError)
                self.listingsPublsisher.onNext([])
            }
        }
    }
}

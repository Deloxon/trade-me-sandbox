# Trade me sandbox

App that consumes some endpoints in the Trade Me API.

It navigates categories and displays the 20 first listings in it.

## Getting Started

### Installing

Make sure to install the pods by running:

```
Pod install
```
## Authors

* **Jose Solorzano** 

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
